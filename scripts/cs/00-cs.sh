#!/bin/bash -x
### https://gitlab.com/container-scripts/cs#installation

# install dependencies
apt install --yes \
    git \
    make \
    m4

# clone the ds repo
git clone \
    https://gitlab.com/container-scripts/cs \
    /opt/container-scripts/cs

# install
cd /opt/container-scripts/cs/
make install
cs
