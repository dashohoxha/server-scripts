#!/bin/bash -x

# remove all docker-scripts containers
for dir in /var/cs/*; do
    cd $dir
    cs remove
done
rm -rf /var/cs/

# uninstall container-scripts
cd /opt/container-scripts/cs/
make uninstall

# remove all the scripts
cd
rm -rf /opt/container-scripts/
