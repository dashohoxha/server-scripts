#!/bin/bash -x
### https://github.com/containers/podman-compose

# exit if podman-compose is already installed
hash podman-compose 2>/dev/null && exit

# install the latest stable version
apt install --yes python3-pip
pip3 install podman-compose
