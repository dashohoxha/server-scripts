#!/bin/bash -x
### https://podman.io/getting-started/installation

# exit if podman is already installed
hash podman 2>/dev/null && exit

# use buster-backports on Debian 10 for a newer libseccomp2
echo 'deb http://deb.debian.org/debian buster-backports main' \
     > /etc/apt/sources.list.d/buster-backports.list

# add kubic repo
echo 'deb https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/Debian_10/ /' \
     > /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list

# get the key pf the kubic repo
apt install --yes \
    curl \
    gnupg2
curl -L https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/Debian_10/Release.key \
    | apt-key add -

# install libseccomp2 and podman
apt update
apt -y -t buster-backports install libseccomp2
apt install --yes podman

podman --version
