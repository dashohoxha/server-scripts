#!/bin/bash -x
### https://docs.docker.com/compose/install/
### https://github.com/docker/compose/releases

# exit if docker-compose is already installed
hash docker-compose 2>/dev/null && exit

release=${1:-1.27.4}

# download docker-compose
curl -L "https://github.com/docker/compose/releases/download/$release/docker-compose-$(uname -s)-$(uname -m)" \
     -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
docker-compose --version
