#!/bin/bash -x
### https://docs.docker.com/engine/install/debian/

# exit if docker is already installed
hash docker 2>/dev/null && exit

# install needed packages
apt update
apt install --yes \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

# add docker’s official gpg key
curl -fsSL https://download.docker.com/linux/debian/gpg \
    | apt-key add -

# add the docker stable repo
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"

# install docker
apt update
apt install --yes \
    docker-ce \
    docker-ce-cli \
    containerd.io
docker --version
