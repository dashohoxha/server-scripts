#!/bin/bash -x

# update/upgrade
apt update
apt upgrade --yes

# make sure that bash-completion is installed
apt install --yes bash-completion

# customize ~/.bashrc
sed -i ~/.bashrc \
    -e '/^#\?force_color_prompt=/ c force_color_prompt=yes' \
    -e '/bashrc_custom/d'
echo 'source ~/.bashrc_custom' >> ~/.bashrc
cat <<'EOF' > ~/.bashrc_custom
# set a better prompt
PS1='${debian_chroot:+($debian_chroot)}\[\033[01;31m\]\u\[\033[01;33m\]@\[\033[01;36m\]\h \[\033[01;33m\]\w \[\033[01;35m\]\$ \[\033[00m\]'

# enable programmable completion features
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    source /etc/bash_completion
fi
EOF

# install firewalld
apt install --yes firewalld
firewall-cmd --permanent --zone=public --set-target=DROP
firewall-cmd --reload

# install fail2ban
apt install --yes fail2ban
