#!/bin/bash

[[ $1 == '--help' ]] && cat <<EOF && exit 0
Usage: $(basename $0) \\
           ssl_cert_email=user@mail.com \\
           `# optional, for sending logwatch notifications` \\
           smtp_server=smtp.example.org \\
           smtp_domain=example.org \\
           logwatch_email=user@mail.com

EOF
set -x

# set some default values for the variables
ssl_cert_email=user@mail.com

# create variables from the named arguments
(($# > 0)) && declare "$@"

# install wordpress
ds pull wordpress
ds init wordpress @wordpress
cd /var/ds/wordpress/

# ssl_cert_email
sed -i settings.sh \
    -e "/^#\?SSL_CERT_EMAIL=/ c SSL_CERT_EMAIL='$ssl_cert_email'"

# logwatch settings
if [[ -n $smtp_server ]]; then
    sed -i settings.sh \
        -e "/^#\?SMTP_SERVER=/ c SMTP_SERVER=$smtp_server" \
        -e "/^#\?SMTP_DOMAIN=/ c SMTP_DOMAIN=$smtp_domain"
else
    sed -i settings.sh \
        -e "/^#\?SMTP_SERVER=/ c #SMTP_SERVER=smtp.example.org" \
        -e "/^#\?SMTP_DOMAIN=/ c #SMTP_DOMAIN=example.org"
fi
[[ -n $logwatch_email ]] && \
    sed -i settings.sh \
	-e "/^#\?LOGWATCH_EMAIL=/ c LOGWATCH_EMAIL=$logwatch_email"

# make
ds make
