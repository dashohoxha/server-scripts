#!/bin/bash

[[ $1 == '--help' ]] && cat <<EOF && exit 0
Usage: $(basename $0) \\
           `# optional, for sending logwatch notifications` \\
           smtp_server=smtp.example.org \\
           smtp_domain=example.org \\
           logwatch_email=user@mail.com

EOF
set -x

# create variables from the named arguments
(($# > 0)) && declare "$@"

# install nsd
ds pull nsd
ds init nsd @nsd
cd /var/ds/nsd/

# logwatch settings
if [[ -n $smtp_server ]]; then
    sed -i settings.sh \
        -e "/^#\?SMTP_SERVER=/ c SMTP_SERVER=$smtp_server" \
        -e "/^#\?SMTP_DOMAIN=/ c SMTP_DOMAIN=$smtp_domain"
else
    sed -i settings.sh \
        -e "/^#\?SMTP_SERVER=/ c #SMTP_SERVER=smtp.example.org" \
        -e "/^#\?SMTP_DOMAIN=/ c #SMTP_DOMAIN=example.org"
fi
[[ -n $logwatch_email ]] && \
    sed -i settings.sh \
	-e "/^#\?LOGWATCH_EMAIL=/ c LOGWATCH_EMAIL=$logwatch_email"

ds make
