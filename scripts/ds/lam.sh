#!/bin/bash

# check that there are some arguments
(($# == 0)) && cat <<EOF >&2 && exit 1
Usage: $(basename $0) \\
           domain=example.org \\
           ssl_cert_email=user@mail.com

EOF
set -x

# set some default values for the variables
domain=fsfe.fs.al
ssl_cert_email=dashohoxha@gmail.com

# create variables from the named arguments
(($# > 0)) && declare "$@"

# install lam
ds pull lam
ds init lam @lam.$domain
cd /var/ds/lam.$domain/
sed -i settings.sh \
    -e "/^#\?CONTAINER=/ c CONTAINER='lam.$domain'" \
    -e "/^#\?DOMAIN=/ c DOMAIN='lam.$domain'" \
    -e "/^#\?SSL_CERT_EMAIL=/ c SSL_CERT_EMAIL='$ssl_cert_email'"
ds make
