#!/bin/bash -x

# make sure that ownership is correct
chown root: -R /opt/docker-scripts/
chown root: -R /var/ds/

# install ds
cd /opt/docker-scripts/ds/
make install

# start docker and make each app
systemctl start docker
for dir in /var/ds/*; do
    cd $dir
    ds make
done
