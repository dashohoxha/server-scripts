#!/bin/bash

# check that there are some arguments
(($# == 0)) && cat <<EOF >&2 && exit 1
Usage: $(basename $0) \\
           domain=example.org \\
           admin_pass='pass123' \\
           ssl_cert_email=user@mail.com \\
           organization='Example Org'

EOF
set -x

# set some default values for the variables
domain='fsfe.fs.al'
admin_pass='pass1234'
ssl_cert_email='dashohoxha@gmail.com'
organization='Free Software Foundation Europe'

# create variables from the named arguments
(($# > 0)) && declare "$@"

# install openldap
ds pull openldap
ds init openldap @ldap.$domain
cd /var/ds/ldap.$domain/
sed -i settings.sh \
    -e "/^#\?CONTAINER=/ c CONTAINER='ldap.$domain'" \
    -e "/^#\?LDAP_DOMAIN=/ c LDAP_DOMAIN='$domain'" \
    -e "/^#\?LDAP_ORGANISATION=/ c LDAP_ORGANISATION='$organization'" \
    -e "/^#\?LDAP_ADMIN_PASSWORD=/ c LDAP_ADMIN_PASSWORD='$admin_pass'" \
    -e "/^#\?DOMAIN=/ c DOMAIN='ldap.$domain'" \
    -e "/^#\?SSL_CERT_EMAIL=/ c SSL_CERT_EMAIL='$ssl_cert_email'"
ds make
