#!/bin/bash -x

# get the latest version of scripts
for dir in /opt/docker-scripts/*; do
    cd $dir
    git pull
done

# update docker-scripts
cd /opt/docker-scripts/ds/
make install

# get the latest version of ubuntu image
docker pull ubuntu:20.04

# update each application container
for dir in /var/ds/*; do
    cd $dir
    ds make
done
