#!/bin/bash

# check that there are some arguments
(($# == 0)) && cat <<EOF >&2 && exit 1
Usage: $(basename $0) \\
           domain=smtp.example.org \\
           mail_domain=example.org \\
           ssl_cert_email=user@mail.com \\
           forward_address=user@mail.com \\
           \`# optional LDAP settings\` \\
           ldap_server_host='ldap.example.org' \\
           ldap_search_base='ou=users,dc=example,dc=org' \\
           ldap_bind_dn='cn=smtp,ou=apps,dc=example,dc=org' \\
           ldap_bind_pw='pass123'

EOF
set -x

# set some default values for the variables
domain='mail1.fsfe.fs.al'
mail_domain='fsfe.fs.al'
ssl_cert_email='dashohoxha@gmail.com'
forward_address=$ssl_cert_email

# create variables from the named arguments
(($# > 0)) && declare "$@"

# install postfix
ds pull postfix
ds init postfix @$domain
cd /var/ds/$domain/

sed -i settings.sh \
    -e "/^#\?CONTAINER=/ c CONTAINER=$domain" \
    -e "/^#\?DOMAIN=/ c DOMAIN=$domain" \
    -e "/^#\?MAIL_DOMAIN=/ c MAIL_DOMAIN=$mail_domain" \
    -e "/^#\?SSL_CERT_EMAIL=/ c SSL_CERT_EMAIL=$ssl_cert_email" \
    -e "/^#\?FORWARD_ADDRESS=/ c FORWARD_ADDRESS=$forward_address"

if [[ -n $ldap_server_host ]]; then
    sed -i settings.sh \
        -e "/^#\?LDAP_SERVER_HOST=/ c LDAP_SERVER_HOST='$ldap_server_host'" \
        -e "/^#\?LDAP_SEARCH_BASE=/ c LDAP_SEARCH_BASE='$ldap_search_base'" \
        -e "/^#\?LDAP_BIND_DN=/ c LDAP_BIND_DN='$ldap_bind_dn'" \
        -e "/^#\?LDAP_BIND_PW=/ c LDAP_BIND_PW='$ldap_bind_pw'"
else
    sed -i settings.sh \
        -e "/^#\?LDAP_SERVER_HOST=/ c #LDAP_SERVER_HOST='ldap.example.org'" \
        -e "/^#\?LDAP_SEARCH_BASE=/ c #LDAP_SEARCH_BASE='ou=users,dc=example,dc=org'" \
        -e "/^#\?LDAP_BIND_DN=/ c #LDAP_BIND_DN='cn=mailserver,dc=example,dc=org'" \
        -e "/^#\?LDAP_BIND_PW=/ c #LDAP_BIND_PW='pass123'"
fi

ds make
