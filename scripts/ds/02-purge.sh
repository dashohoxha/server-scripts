#!/bin/bash -x

# remove all docker-scripts containers
for dir in /var/ds/*; do
    cd $dir
    ds remove
done
rm -rf /var/ds/

# uninstall docker-scripts
cd /opt/docker-scripts/ds/
make uninstall

# remove all the scripts
cd
rm -rf /opt/docker-scripts/
