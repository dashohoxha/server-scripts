#!/bin/bash

[[ $1 == '--help' ]] && cat <<EOF && exit 0
Usage: $(basename $0) \\
           `# optional, for sending logwatch notifications` \\
           smtp_server=smtp.example.org \\
           smtp_domain=example.org

EOF
set -x

# create variables from the named arguments
(($# > 0)) && declare "$@"

# install lam
ds pull wsproxy
ds init wsproxy @wsproxy
cd /var/ds/wsproxy/

if [[ -n $smtp_server ]]; then
    sed -i settings.sh \
        -e "/^#\?SMTP_SERVER=/ c SMTP_SERVER=$smtp_server" \
        -e "/^#\?SMTP_DOMAIN=/ c SMTP_DOMAIN=$smtp_domain"
else
    sed -i settings.sh \
        -e "/^#\?SMTP_SERVER=/ c #SMTP_SERVER=smtp.example.org" \
        -e "/^#\?SMTP_DOMAIN=/ c #SMTP_DOMAIN=example.org"
fi

ds make
