#!/bin/bash -x

# install dependencies
apt install --yes \
    git \
    make \
    m4

# clone the ds repo
git clone \
    https://gitlab.com/docker-scripts/ds \
    /opt/docker-scripts/ds

# install
cd /opt/docker-scripts/ds/
make install
ds
