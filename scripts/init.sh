#!/bin/bash -x

# update/upgrade
apt update
apt -y upgrade

# enable ls colors
sed -i ~/.bashrc \
    -e 's/^# export LS_OPTIONS/export LS_OPTIONS/' \
    -e '/dircolors/ s/^# eval/eval/' \
    -e 's/^# alias ls=/alias ls=/' \
    -e 's/^# alias ll=/alias ll=/' \
    -e 's/^# alias l=/alias l=/'

# set a better prompt
sed -i ~/.bashrc -e '/^PS1=/d'
cat <<'EOF' >> ~/.bashrc
PS1='${debian_chroot:+($debian_chroot)}\[\033[01;31m\]\u\[\033[01;33m\]@\[\033[01;36m\]\h \[\033[01;33m\]\w \[\033[01;35m\]\$ \[\033[00m\]'
EOF

# install firewalld
apt install --yes firewalld
firewall-cmd --permanent --zone=public --set-target=DROP
firewall-cmd --reload

# install fail2ban
apt install --yes fail2ban
