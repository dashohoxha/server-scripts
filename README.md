
# Scripts for automating server installation and maintenance

To install/setup server `meitner` run `server/meitner install`.

You need an entry for `meitner` on `hosts` and the SSH key
`keys/meitner.key`. Test access to the server with `bin/ssh meitner`.

Other actions are: `server/meitner update`, `server/meitner cleanup`,
etc.
