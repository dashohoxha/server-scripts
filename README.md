
# Scripts for automating server installation and maintenance

To install/setup server `lab1` run `server/lab1 install`.

You need an entry for `lab1` on `hosts` and the SSH key
`keys/lab1.key`. Test access to the server with `bin/ssh lab1`.

Other actions are: `server/lab1 update`, `server/lab1 cleanup`, etc.
