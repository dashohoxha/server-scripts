#!/bin/bash -x

domain=${1:-example.org}

# install ds
cd /opt/docker-scripts/ds/
make install

# make sure that ownership is correct
chown root: -R /opt/docker-scripts/
chown root: -R /var/ds/

# SMTP
cd /var/ds/smtp.$domain/
ds make

# LDAP
cd /var/ds/ldap.$domain/
ds exec chown -R openldap: sslcert/ slapd.d/ data/
ds make
ds restore $(ls -t backup-*.tgz | head -1)

# LAM
cd /var/ds/lam.$domain/
ds exec chown www-data: -R config/
ds make
