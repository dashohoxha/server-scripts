#!/bin/bash -x

domain=${1:-example.org}
app_list="lam.$domain ldap.$domain smtp.$domain"

for app in $app_list ; do
    cd /var/ds/$app/
    ds remove
done

cd /var/ds/
rm -rf $app_list
