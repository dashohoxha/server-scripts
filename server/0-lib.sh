
usage() {
    cat <<EOF

Usage: $0 [install | testing | update | backup | restore | cleanup]

EOF
}

main() {
    local action=$1 ; shift
    case $action in
        install|testing|update|backup|restore|cleanup)
            $action "$@"
            ;;
        '')
            usage
            exit 0
            ;;
        *)
            usage >&2
            exit 1
            ;;
    esac
}

install() {
    echo 'Not implemented yet.' >&2
    exit 1
}

testing() {
    echo 'Not implemented yet.' >&2
    exit 1
}

update() {
    echo 'Not implemented yet.' >&2
    exit 1
}

backup() {
    echo 'Not implemented yet.' >&2
    exit 1
}

restore() {
    echo 'Not implemented yet.' >&2
    exit 1
}

cleanup() {
    echo 'Not implemented yet.' >&2
    exit 1
}


HOST=$(basename $0)

ssh() {
    echo "===> ssh -qF hosts $HOST $@"
    /usr/bin/ssh -qF hosts $HOST "$@"
}

scp() {
    echo "===> scp -qF hosts $@"
    /usr/bin/scp -qF hosts "$@"
}

rsync() {
    echo "===> rsync -e 'ssh -qF hosts' $@"
    /usr/bin/rsync -e 'ssh -qF hosts' "$@"
}

rexec() {
    local script=$1 ; shift
    if [[ -z $script ]]; then
        cat | ssh 'bash -sx'
    else
        ssh 'bash -sx' < $script "$@"
    fi
}

run() {
    local script=$1 ; shift
    [[ -z $script ]] && return
    script_name=$(basename $script)
    ssh rm -f /tmp/$script_name
    scp $script $HOST:/tmp/
    ssh /tmp/$script_name "$@"
    ssh rm -f /tmp/$script_name
}
