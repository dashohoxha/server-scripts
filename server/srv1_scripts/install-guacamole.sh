#!/bin/bash

ds pull guacamole
ds init guacamole @guac.example.org
cd /var/ds/guac.example.org/

cat <<EOF > settings.sh
APP=guacamole
IMAGE="dockerscripts/guacamole"
CONTAINER="guacamole"
DOMAIN="guac.example.org"
SSL_CERT_EMAIL=user@mail.com

ADMIN="admin"
PASS="pass123"

DBHOST=mariadb    # this is the name of the mariadb container
DBPORT=3306
DBNAME=guacamole
DBUSER=guacamole
DBPASS=pass123

SMTP_SERVER="smtp.example.org"
SMTP_DOMAIN="example.org"
LOGWATCH_EMAIL="user@mail.com"
EOF

# make
ds make
