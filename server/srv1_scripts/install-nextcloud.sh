#!/bin/bash

ds pull nextcloud
ds init nextcloud @cloud.example.org
cd /var/ds/cloud.example.org/

cat <<EOF > settings.sh
APP=nextcloud
IMAGE=nextcloud
CONTAINER=cloud.example.org
DOMAIN="cloud.example.org"
SSL_CERT_EMAIL="user@mail.com"

ADMIN_USER=admin
ADMIN_PASS=pass123
ADMIN_EMAIL="user@mail.com"

DBHOST=mariadb
DBPORT=3306
DBNAME=nc_example_org
DBUSER=nc_example_org
DBPASS=pass123

### SMTP server used for sending notification emails.
SMTP_SERVER=smtp.example.org
SMTP_DOMAIN=example.org
EOF

ds make
