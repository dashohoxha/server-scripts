#!/bin/bash -x

# linuxmint
cd /var/ds/linuxmint/
ds restore $(ls -t backup/users-*.tgz | head -1)

# guac.example.org
cd /var/ds/guac.example.org/
ds restore $(ls -t backup-*.tgz | head -1)

# wordpress
cd /var/ds/wordpress/
for site in site1.example.org site2.example.org ; do
    ds restore $(ls -t backup/wordpress-$site-*.tgz | head -1)
done

# discourse
cd /var/ds/forum.example.org/
ds restore $(ls backup/*.tar.gz -t | head -1)
