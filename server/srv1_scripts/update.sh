#!/bin/bash -x

# get the latest version of scripts
for dir in /opt/docker-scripts/* ; do
    cd $dir
    git pull
done

# update ds
cd /opt/docker-scripts/ds/
make install

# get the latest version of ubuntu image
docker pull ubuntu:20.04

# run 'ds make' on each of these apps
for app in wsproxy mariadb nsd wg1 wg2 wordpress minio.fs.al git.example.org ; do
    cd /var/ds/$app/
    ds make
done

# nextcloud
cd /var/ds/cloud.example.org/
ds update
ds occ upgrade

# linuxmint
docker pull ubuntu:18.04
cd /var/ds/linuxmint/
ds remake

# guacamole
cd /var/ds/guac.example.org/
ds backup
ds make
ds restore $(ls -t backup-*.tgz | head -1)

# discourse
cd /var/ds/forum.example.org/
ds upgrade

# remove dangling images
docker image prune --force
docker image prune --force

# update the system
apt update
apt upgrade --yes
reboot
