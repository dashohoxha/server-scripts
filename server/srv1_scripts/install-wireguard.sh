#!/bin/bash

ds pull wireguard

ds init wireguard @wg1
cd /var/ds/wg1/
sed -i settings.sh \
    -e '/CONTAINER/ c CONTAINER=wg1'
ds make

ds init wireguard @wg2
cd /var/ds/wg2/
sed -i settings.sh \
    -e '/CONTAINER/ c CONTAINER=wg2' \
    -e '/ROUTED_NETWORKS/ c ROUTED_NETWORKS="10.10.10.0/24"' \
    -e '/ALLOW_INTERNET_ACCESS/ c ALLOW_INTERNET_ACCESS=no' \
    -e '/CLIENT_TO_CLIENT/ c CLIENT_TO_CLIENT=yes' \
    -e '/^#\?KEEPALIVE_PERIOD/ c KEEPALIVE_PERIOD=25'
ds make
