#!/bin/bash -x

ds pull discourse
ds init discourse @forum.example.org
cd /var/ds/forum.example.org/
cat <<EOF > settings.sh
APP=discourse
DOMAIN="forum.example.org"
CONTAINER="forum.example.org"

ADMIN_EMAIL="user@mail.com"

SMTP_SERVER=smtp.example.org
SMTP_PORT=25
SMTP_USER=
SMTP_PASS=
SMTP_TLS=true
EOF
ds make

cat <<EOF > discourse_docker/containers/mail-receiver.yml
base_image: discourse/mail-receiver:release
update_pups: false

expose:
  - "25:25"   # SMTP

env:
  LANG: en_US.UTF-8
  MAIL_DOMAIN: forum.example.org
  DISCOURSE_MAIL_ENDPOINT: 'https://forum.example.org/admin/email/handle_mail'
  ## You can get the API_KEY from the "API" tab of your admin panel.
  DISCOURSE_API_KEY: 667ad40ic7a0e4bf004caf671ef84140e31a514b15ed0e8aff50458e3e4736e6
  DISCOURSE_API_USERNAME: system

volumes:
  - volume:
      host: /var/ds/forum.example.org/discourse_docker/shared/mail-receiver/postfix-spool
      guest: /var/spool/postfix

EOF
discourse_docker/launcher rebuild mail-receiver
