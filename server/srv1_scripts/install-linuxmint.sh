#!/bin/bash

ds pull linuxmint
ds init linuxmint @linuxmint
cd /var/ds/linuxmint/

cat <<EOF > settings.sh
APP=linuxmint
IMAGE="dockerscripts/linuxmint:19.3"
CONTAINER="linuxmint"

EPOPTES_USERS="teacher"
ADMIN_USER="admin"
ADMIN_PASS="pass123"

SMTP_SERVER="smtp.example.org"
SMTP_DOMAIN="example.org"
LOGWATCH_EMAIL="user@mail.com"
EOF

cat <<EOF > accounts.txt
user1:pass1
user2:pass2
user3:pass3
EOF

# make
ds make
