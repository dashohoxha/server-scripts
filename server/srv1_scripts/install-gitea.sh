#!/bin/bash -x

ds pull gitea
ds init gitea @git.example.org
cd /var/ds/git.example.org/

cat <<EOF > settings.sh
APP=gitea

### Docker settings.
IMAGE=gitea
CONTAINER=git.example.org
DOMAIN=git.example.org
SSL_CERT_EMAIL=user@mail.com

SMTP_SERVER="smtp.example.org"
SMTP_DOMAIN="example.org"

### Application name, used in the page title.
APP_NAME="Gitea: Git with a cup of tea"

### Create an admin user. It should be different from "admin" because this
### is a reserved named by Gitea.
ADMIN_USER="gitadmin"
ADMIN_PASS="pass123"
ADMIN_EMAIL="user@mail.com"

DBHOST=mariadb
DBPORT=3306
DBNAME=git_example_org
DBUSER=git_example_org
DBPASS=pass123
EOF

ds make
